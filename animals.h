// animals.h
// This file contains the declaration of the Zoo class and its associated data structures.
// It defines the structure of the animal and breed nodes and the member functions of the Zoo class.

struct AnimalNode {
    char *name;             // Name of the animal
    int yearsInCaptivity;   // Number of years the animal has been in captivity
    char *origin;           // Origin of the animal
    char *favToy;           // Favorite toy of the animal
    AnimalNode *next;       // Pointer to the next animal node in the linked list
};

struct BreedNode {
    char *name;             // Name of the breed
    char *description;      // Description of the breed
    char *naturalHabitat;   // Natural habitat of the breed
    char *countryOfOrigin;  // Country of origin of the breed
    AnimalNode *head;       // Pointer to the first animal node associated with the breed
    BreedNode *next;        // Pointer to the next breed node in the linked list
};

class Zoo {
private:
    BreedNode *head;        // Pointer to the first breed node in the linked list

public:
    Zoo();                  // Constructor for the Zoo class
    ~Zoo();                 // Destructor for the Zoo class

    // Display animals by breed
    // Displays all animals of a specific breed
    // Returns true if the breed exists and has animals, false otherwise
    bool displayAnimalsByBreed(const char *nameOfBreed);

    // Add a new animal breed
    // Adds a new breed to the breed linked list
    // Returns true if the breed is added successfully, false otherwise
    bool addBreed(const char *name, const char *description, const char *naturalHabitat, const char *countryOfOrigin);

    // Add a new animal to a specific breed
    // Adds a new animal to the animal linked list of a specific breed
    // Returns true if the animal is added successfully, false otherwise
    bool addAnimal(const char *nameOfBreed, char *nameOfAnimal, int yearsInCaptivity, char *origin, char *favToy);

    // Display all animal breeds
    // Displays information about all animal breeds in the zoo
    // Returns true if there are breeds to display, false otherwise
    bool displayAllBreeds();

    // Display animals by age range
    // Displays all animals within a specific age range
    // Returns true if there are animals within the age range, false otherwise
    bool displayAnimalsByAge(int lowAge, int highAge);

    // Remove an animal breed
    // Removes a breed and all its associated animals from the zoo
    // Returns true if the breed is removed successfully, false otherwise
    bool removeBreed(const char *name);

    // Retrieve information about a specific animal breed
    // Returns true if the breed exists in the zoo, false otherwise
    bool retrieveBreed(const char *name);
};