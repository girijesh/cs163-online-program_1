// animals.cpp
// This file contains the implementation of the Zoo class and its member functions.
// It manages the animal breeds and animals in the zoo using linked lists.

#include "animals.h"
#include <cstring>
#include <iostream>
using namespace std;

// Constructor
// Initializes the head of the breed linked list to NULL.
Zoo::Zoo()
{
    head = NULL;
}

// Destructor

Zoo::~Zoo() {
    BreedNode* currentBreed = head;
    while (currentBreed != NULL) {
        BreedNode* tempBreed = currentBreed;
        currentBreed = currentBreed->next;

        AnimalNode* currentAnimal = tempBreed->head;
        while (currentAnimal != NULL) {
            AnimalNode* tempAnimal = currentAnimal;
            currentAnimal = currentAnimal->next;

            // Free memory allocated for animal data
            delete[] tempAnimal->name;
            delete[] tempAnimal->origin;
            delete[] tempAnimal->favToy;
            delete tempAnimal;
        }

        // Free memory allocated for breed data
        delete[] tempBreed->name;
        delete[] tempBreed->description;
        delete[] tempBreed->naturalHabitat;
        delete[] tempBreed->countryOfOrigin;
        delete tempBreed;
    }
}

// Add a new Animal breed
// Adds a new breed to the breed linked list.
// Returns true if the breed is added successfully, false otherwise.
bool Zoo::addBreed(const char *name, const char *description, const char *naturalHabitat, const char *countryOfOrigin)
{
    // Check if the breed already exists
    if (retrieveBreed(name))
    {
        cout << "Error: Breed already exists in the list." << endl;
        return false;
    }

    // Create a new BreedNode
    BreedNode *newBreed = new BreedNode;
    newBreed->name = new char[strlen(name) + 1];
    strcpy(newBreed->name, name);
    newBreed->description = new char[strlen(description) + 1];
    strcpy(newBreed->description, description);
    newBreed->naturalHabitat = new char[strlen(naturalHabitat) + 1];
    strcpy(newBreed->naturalHabitat, naturalHabitat);
    newBreed->countryOfOrigin = new char[strlen(countryOfOrigin) + 1];
    strcpy(newBreed->countryOfOrigin, countryOfOrigin);
    newBreed->head = NULL;
    newBreed->next = nullptr;

    // Add the new breed to the beginning of the list
    newBreed->next = head;
    head = newBreed;

    return true;
}

// Add a new animal to a specific breed
// Adds a new animal to the animal linked list of a specific breed.
// Returns true if the animal is added successfully, false otherwise.
bool Zoo::addAnimal(const char *nameOfBreed, char *nameOfAnimal, int yearsInCaptivity, char *origin, char *favToy)
{
    if (!retrieveBreed(nameOfBreed))
    {
        cout << "Error: Breed doesn't exist in list." << endl;
        return false;
    }

    // Create a new AnimalNode
    AnimalNode *newAnimal = new AnimalNode;
    newAnimal->name = new char[strlen(nameOfAnimal) + 1];
    strcpy(newAnimal->name, nameOfAnimal);
    newAnimal->yearsInCaptivity = yearsInCaptivity;
    newAnimal->origin = new char[strlen(origin) + 1];
    strcpy(newAnimal->origin, origin);
    newAnimal->favToy = new char[strlen(favToy) + 1];
    strcpy(newAnimal->favToy, favToy);
    newAnimal->next = NULL;

    // Find the breed node to add the animal to
    BreedNode *current = head;
    while (strcmp(current->name, nameOfBreed) != 0)
    {
        current = current->next;
    }

    // Add the animal to the breed's animal list in alphabetical order
    if (current->head == NULL)
    {
        current->head = newAnimal;
    }
    else
    {
        AnimalNode *prev = NULL;
        AnimalNode *curr = current->head;

        while (curr != NULL && strcmp(curr->name, nameOfAnimal) < 0)
        {
            prev = curr;
            curr = curr->next;
        }

        if (prev == NULL)
        {
            newAnimal->next = current->head;
            current->head = newAnimal;
        }
        else
        {
            newAnimal->next = curr;
            prev->next = newAnimal;
        }
    }
    return true;
}

// Display all animal breeds
// Displays information about all animal breeds in the zoo.
// Returns true if there are breeds to display, false otherwise.
bool Zoo::displayAllBreeds()
{
    if (head == nullptr)
    {
        cout << "Error: There are no animals in the list." << endl;
        return false;
    }

    BreedNode *current = head;
    while (current != nullptr)
    {
        cout << "Breed: " << current->name << endl;
        cout << "Description: " << current->description << endl;
        cout << "Natural Habitat: " << current->naturalHabitat << endl;
        cout << "Country Of Origin: " << current->countryOfOrigin << endl;
        cout << endl;
        current = current->next;
    }

    return true;
}

// Display animals by breed
// Displays all animals of a specific breed.
// Returns true if the breed exists and has animals, false otherwise.
bool Zoo::displayAnimalsByBreed(const char *nameOfBreed)
{
    if (!retrieveBreed(nameOfBreed))
    {
        cout << "Error: Breed doesn't exists in the list." << endl;
        return false;
    }

    BreedNode *current = head;
    while (strcmp(current->name, nameOfBreed) != 0)
    {
        current = current->next;
    }

    AnimalNode *animal = current->head;

    if (animal == NULL)
    {
        cout << "No animals found for the breed" << endl;
    }
    else
    {
        cout << "Animals for the breed: " << nameOfBreed << endl;
        while (animal != NULL)
        {
            cout << "Name: " << animal->name << endl;
            cout << "Years in Captivity: " << animal->yearsInCaptivity << endl;
            cout << "Origin: " << animal->origin << endl;
            cout << "Favorite Toy: " << animal->favToy << endl;
            cout << "------------------------" << endl;
            animal = animal->next;
        }
    }
    return true;
}

// Display animals by age range
// Displays all animals within a specific age range.
// Returns true if there are animals within the age range, false otherwise.
bool Zoo::displayAnimalsByAge(int lowAge, int highAge)
{
    if (lowAge > highAge)
    {
        cout << "Error: Low age should be less than or equal to high age." << endl;
        return false;
    }

    bool found = false;

    BreedNode *currentBreed = head;
    while (currentBreed != NULL)
    {
        AnimalNode *currentAnimal = currentBreed->head;
        while (currentAnimal != NULL)
        {
            if (currentAnimal->yearsInCaptivity >= lowAge && currentAnimal->yearsInCaptivity <= highAge)
            {
                if (!found)
                {
                    cout << "Animals within the age range " << lowAge << " to " << highAge << ":" << endl;
                    found = true;
                }
                cout << "Breed: " << currentBreed->name << endl;
                cout << "Name: " << currentAnimal->name << endl;
                cout << "Years in Captivity: " << currentAnimal->yearsInCaptivity << endl;
                cout << "Origin: " << currentAnimal->origin << endl;
                cout << "Favorite Toy: " << currentAnimal->favToy << endl;
                cout << "------------------------" << endl;
            }
            currentAnimal = currentAnimal->next;
        }
        currentBreed = currentBreed->next;
    }

    if (!found)
    {
        cout << "No animals found within the age range " << lowAge << " to " << highAge << endl;
    }

    return true;
}

// Remove an animal breed
// Removes a breed and all its associated animals from the zoo.
// Returns true if the breed is removed successfully, false otherwise.
bool Zoo::removeBreed(const char *nameOfBreed)
{
    if (!retrieveBreed(nameOfBreed))
    {
        cout << "Error: Breed does not exist in the list." << endl;
        return false;
    }

    BreedNode *current = head;
    BreedNode *prev = NULL;

    while (current != NULL && strcmp(current->name, nameOfBreed) != 0)
    {
        prev = current;
        current = current->next;
    }

    if (current == NULL)
    {
        cout << "Error: Breed not found." << endl;
        return false;
    }

    // Remove all animals associated with the breed
    AnimalNode *animalCurrent = current->head;
    while (animalCurrent != NULL)
    {
        AnimalNode *temp = animalCurrent;
        animalCurrent = animalCurrent->next;
        delete[] temp->name;
        delete[] temp->origin;
        delete[] temp->favToy;
        delete temp;
    }

    // Remove the breed node from the linked list
    if (prev == NULL)
    {
        // Breed is the head of the list
        head = current->next;
    }
    else
    {
        prev->next = current->next;
    }

    delete[] current->name;
    delete[] current->description;
    delete[] current->naturalHabitat;
    delete[] current->countryOfOrigin;
    delete current;

    cout << "Breed '" << nameOfBreed << "' and its animals have been removed." << endl;

    return true;
}

// Helper function to retrieve a breed by name
// Returns true if the breed exists in the zoo, false otherwise.
bool Zoo::retrieveBreed(const char *name)
{
    BreedNode *current = head;
    while (current != NULL)
    {
        if (strcmp(current->name, name) == 0)
        {
            return true;
        }
        current = current->next;
    }
    return false;
}