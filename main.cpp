// Girijesh Thodupunuri
// CS 163 
// Program 1


// main.cpp
// This file contains the main function and the user interface for the Zoo Management System.
// It interacts with the user, displays a menu, and calls the appropriate functions based on user input.

#include "animals.h"
#include <iostream>
using namespace std;

// Function to display the menu options
void displayMenu() {
    cout << "Zoo Management System" << endl;
    cout << "1. Add Animal Breed" << endl;
    cout << "2. Add Animal" << endl;
    cout << "3. Display All Animal Breeds" << endl;
    cout << "4. Display Animals for a Breed" << endl;
    cout << "5. Display Animals by Age Range" << endl;
    cout << "6. Remove Animal Breed" << endl;
    cout << "0. Exit" << endl;
    cout << "Enter your choice: ";
}

int main() {
    Zoo zoo;  // Create an instance of the Zoo class

    int choice;  // Variable to store user's menu choice

    // Variables to store user input
    char name[100], description[100], naturalHabitat[100], countryOfOrigin[100];
    char nameOfBreed[100], nameOfAnimal[100], origin[100], favToy[100];
    int yearsInCaptivity, lowAge, highAge;

    do {
        displayMenu();  // Display the menu options

        cin >> choice;  // Read user's menu choice
        cin.ignore();   // Ignore the newline character

        cout << endl;

        switch (choice) {
            case 1:  // Add Animal Breed
                cout << "Enter breed name: ";
                cin.get(name, 100, '\n');
                cin.ignore();
                cout << "Enter breed description: ";
                cin.get(description, 100, '\n');
                cin.ignore();
                cout << "Enter natural habitat: ";
                cin.get(naturalHabitat, 100, '\n');
                cin.ignore();
                cout << "Enter country of origin: ";
                cin.get(countryOfOrigin, 100, '\n');
                cin.ignore();
                zoo.addBreed(name, description, naturalHabitat, countryOfOrigin);
                break;

            case 2:  // Add Animal
                cout << "Enter breed name: ";
                cin.get(nameOfBreed, 100, '\n');
                cin.ignore();
                cout << "Enter animal name: ";
                cin.get(nameOfAnimal, 100, '\n');
                cin.ignore();
                cout << "Enter years in captivity: ";
                cin >> yearsInCaptivity;
                cin.ignore();
                cout << "Enter origin: ";
                cin.get(origin, 100, '\n');
                cin.ignore();
                cout << "Enter favorite toy: ";
                cin.get(favToy, 100, '\n');
                cin.ignore();
                zoo.addAnimal(nameOfBreed, nameOfAnimal, yearsInCaptivity, origin, favToy);
                break;

            case 3:  // Display All Animal Breeds
                zoo.displayAllBreeds();
                break;

            case 4:  // Display Animals for a Breed
                cout << "Enter breed name: ";
                cin.get(nameOfBreed, 100, '\n');
                cin.ignore();
                zoo.displayAnimalsByBreed(nameOfBreed);
                break;

            case 5:  // Display Animals by Age Range
                cout << "Enter low age: ";
                cin >> lowAge;
                cin.ignore();
                cout << "Enter high age: ";
                cin >> highAge;
                cin.ignore();
                zoo.displayAnimalsByAge(lowAge, highAge);
                break;

            case 6:  // Remove Animal Breed
                cout << "Enter breed name: ";
                cin.get(nameOfBreed, 100, '\n');
                cin.ignore();
                zoo.removeBreed(nameOfBreed);
                break;

            case 0:  // Exit
                cout << "Exiting the program." << endl;
                break;

            default:
                cout << "Invalid choice. Please try again." << endl;
                break;
        }

        cout << endl;
    } while (choice != 0);  // Continue the loop until user chooses to exit

    return 0;
}